
  // navbar and hero animation 
  let navHead = document.getElementById("nav_header");
  let tl1 = gsap.timeline();
if(navHead){

  tl1.from(".logo", 1, {
      opacity: 0,
      x: -20,
      ease: Power3.easeInOut,
    })
  tl1.from(".hamburger", 1, {
      opacity: 0,
      x: -20,
      ease: Power2.easeOut,
      stagger: 0.08
    },"-=0.75");
}

    // index hero section 
    let indexHero = document.querySelector(".hero");
    if(indexHero){
      tl1.from(".hero_content",{
        opacity: 0,
        y: 10,
        ease: Power2.easeOut,
      },'-=0.4')
    }

    /*scroll magic*/
    let ctrl = new ScrollMagic.Controller();
  // Create scenes in jQuery each() loop
  let scrollAnimation = document.querySelector('.animation_main');
  if(scrollAnimation){
    $('.animation_main').each(function(e) {
      let aboutH3 = $(this).find(".des h3");
      let aboutP = $(this).find(".des p");
      let portfolio = $(this).find(".portfolio_item");
      let aboutImg = $(this).find(".img_container img");

    
      let tl2 = gsap.timeline();
      gsap.config({
        nullTargetWarn: false,
      });

      tl2.fromTo(aboutH3, 1, {y:"20%",
       opacity:"0.7",
      }, { y:"0%",
          opacity:"1", 
          stagger: 0.08,
          duration:1,
          ease: Power2.easInOut,},0)
          .fromTo(aboutP, 2, { scale:"0.7", opacity:"0.4"}, {scale:"1", opacity:"1",  ease: Power2.easInOut},'-=0.5')
        tl2.fromTo(aboutImg, 2, { opacity:"0.3", y:"10%"}, { opacity:"1", y:"0%", ease: Power2.easInOut}, 0);

  
          // portfolio section 
          tl2.fromTo(portfolio, 1.5, { y:"10%", stagger: 0.08, opacity:"0.4"}, {y:"0%",stagger: 0.08, opacity:"1",  ease: Power3.easeInOut},0);


      new ScrollMagic.Scene({
        triggerElement: this,
        duration: 0,
        triggerHook: 0.8,
        offset: 1
      })
      
        .setTween(tl2)
        .addTo(ctrl);
        // Scene.addIndicators();
    });
  }
  
  